<?php
/**
 * ErhvervsHjemmesider Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ErhvervsHjemmesider
 * @since 1.0
 */
/**
 * Copy from here to your (child) themes functions.php
 * Recommended to do so via FTP.
 */

/**
 * Rename "home" in breadcrumb
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_home_text' );
function wcc_change_breadcrumb_home_text( $defaults ) {
	$defaults['home'] = 'Postershop';
	return $defaults;
}

/*Allow SVG upload*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/*****/

add_shortcode( 'get_the_year', 'tu_get_the_year' );
function tu_get_the_year() {
    return date( 'Y' );
}

/**
 * Define Constants
 */
define( 'CHILD_THEME_ERHVERVSHJEMMESIDER_VERSION', '1.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'erhvervshjemmesider-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ERHVERVSHJEMMESIDER_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

/**
 * Deaktivering af Kommentarer
 */

add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;
    
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }
    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});
// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);
// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);
// Remove comments page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});
// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});

// * Afslutning af Deaktivering af Kommentarer *// 
// 
// 
// 
// 
function mdshak_cart_url( $example ) {
    // Maybe modify $example in some way.
    return 'https://postershop.dk/kurv/';
}
add_filter( 'woocommerce_add_to_cart_redirect', 'mdshak_cart_url' );
add_filter( 'recovery_mode_email', 'send_sumun_the_recovery_mode_email', 10, 2 );
function send_sumun_the_recovery_mode_email( $email, $url ) {
$email['to'] = 'bobby@postershop.dk';
return $email;
}
